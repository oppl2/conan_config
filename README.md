<!--
 * @Author: Yinjie Lee
 * @Date: 2022-04-21 23:03:16
 * @LastEditors: Yinjie Lee
 * @LastEditTime: 2022-04-21 23:07:25
-->
# Conan Config

A general Conan configuration for any liconan project.

## Introduction

[Conan config](https://docs.conan.io/en/latest/reference/commands/consumer/config.html) was
released on Conan 0.27.0 providing the option to distribute and share remotes, profiles, settings.

This project is a default template for remotes and profiles.
Anyone is able to use and contribute.

### install

To apply this template in your current environment:

    conan config install https://gitlab.com/oppl2/conan_config.git

### Use

After you setup this profiles, conan installs the package under x64, just like this:

    conan install . --install-folder=./build --profile=x64